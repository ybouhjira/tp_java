package exercice3;

import java.util.Scanner;

class Vecteur3d {
// ATTRIBUTS

    private double x;
    private double y;
    private double z;

// METHODES
    /**
     * Constructeur
     *
     * @param x
     * @param y
     * @param z
     */
    public Vecteur3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

// ACCESSEURS ET MODIFICATEURS    
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "<" + "x=" + x + ", y=" + y + ", z=" + z + '>';
    }
}

public class Exercice3 {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.print("Combien de vecteur?\t");
        int count = scn.nextInt();
        Vecteur3d[] vectors = new Vecteur3d[count];
        
        // utiliser tous les caracteres nom numérique comme séparateur
        scn.useDelimiter("[^0-9.+-]*");
        for (int i = 0; i < count; i++) {
            System.out.print("Vecteur " + i + " :\t");
            vectors[i] = new Vecteur3d(scn.nextDouble(), scn.nextDouble(),
                    scn.nextDouble());
        }
        
        for(Vecteur3d vector : vectors) {
            System.out.println(vector);
        }
    }
}
