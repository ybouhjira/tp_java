package exercice3;

class Forme {

    private double x, y;

    public Forme(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void deplace(double dx, double dy) {
        x += dx;
        y += dy;
    }

    public void affiche() {
        System.out.println("Point de coordonnees " + x + " " + y);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}

class Cercle extends Forme {

    private double rayon;

    public Cercle(int rayon, double x, double y) {
        super(x, y);
        this.rayon = rayon;
    }

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }

    @Override
    public void affiche() {
        System.out.println("Centre: (" + getX() + "," + getY() + ")"
                + "rayon: " + rayon);
    }
    
    public double surface() {
        return rayon * Math.pow(Math.PI , 2) ;
    }
    
    public double perimetre() {
        return 2 * rayon * Math.PI ;
    }
}

public class Exercice3 {

    public static void main(String[] args) {
        Cercle c = new Cercle(2, 1, 3);
        c.deplace(1, 1);
        System.out.println("Perimetre : " + c.perimetre());
        System.out.println("Surface : " + c.surface());
        c.affiche();
    }
}
