package exercice4;

import java.math.BigInteger;
import java.util.Scanner;

abstract class ErrCalcul extends Exception {

    private int val1, val2;

    public ErrCalcul(int val1, int val2) {
        this.val1 = val1;
        this.val2 = val2;
    }

    public int getVal1() {
        return val1;
    }

    public void setVal1(int val1) {
        this.val1 = val1;
    }

    public int getVal2() {
        return val2;
    }

    public void setVal2(int val2) {
        this.val2 = val2;
    }
}

class ErrSom extends ErrCalcul {

    public ErrSom(int val1, int val2) {
        super(val1, val2);
    }
}

class ErrDiff extends ErrCalcul {

    public ErrDiff(int val1, int val2) {
        super(val1, val2);
    }
}

class ErrProd extends ErrCalcul {

    public ErrProd(int val1, int val2) {
        super(val1, val2);
    }
}

class EntNat {

    private int value;

    public EntNat(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static EntNat somme(int a, int b) throws ErrSom {
        BigInteger bigA = new BigInteger(String.valueOf(a));
        BigInteger bigB = new BigInteger(String.valueOf(b));
        BigInteger sum = bigA.add(bigB);

        BigInteger max = new BigInteger(String.valueOf(Integer.MAX_VALUE));
        BigInteger min = new BigInteger(String.valueOf(Integer.MIN_VALUE));
        if (sum.compareTo(max) > 0 || sum.compareTo(min) < 0) {
            throw new ErrSom(a, b);
        }
        return new EntNat(Integer.parseInt(sum.toString()));
    }

    public static EntNat difference(int a, int b) throws ErrDiff {
        BigInteger bigA = new BigInteger(String.valueOf(a));
        BigInteger bigB = new BigInteger(String.valueOf(b));
        BigInteger diff = bigA.subtract(bigB);

        BigInteger max = new BigInteger(String.valueOf(Integer.MAX_VALUE));
        BigInteger min = new BigInteger(String.valueOf(Integer.MIN_VALUE));

        if (diff.compareTo(max) > 0 || diff.compareTo(min) < 0) {
            throw new ErrDiff(a, b);
        }
        return new EntNat(Integer.parseInt(diff.toString()));
    }

    public static EntNat produit(int a, int b) throws ErrProd {
        BigInteger bigA = new BigInteger(String.valueOf(a));
        BigInteger bigB = new BigInteger(String.valueOf(b));
        BigInteger prod = bigA.multiply(bigB);

        BigInteger max = new BigInteger(String.valueOf(Integer.MAX_VALUE));
        BigInteger min = new BigInteger(String.valueOf(Integer.MIN_VALUE));

        if (prod.compareTo(max) > 0 || prod.compareTo(min) < 0) {
            throw new ErrProd(a, b);
        }
        return new EntNat(Integer.parseInt(prod.toString()));
    }

    public static EntNat produit(EntNat a, EntNat b) throws ErrProd {
        return produit(a.getValue(), b.getValue());
    }

    public static EntNat somme(EntNat a, EntNat b) throws ErrSom {
        return somme(a.getValue(), b.getValue());
    }

    public static EntNat difference(EntNat a, EntNat b) throws ErrDiff {
        return difference(a.getValue(), b.getValue());
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}

public class Exercice4 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Entrez a : ");
        EntNat a = new EntNat(scanner.nextInt());

        System.out.println("Entrez b : ");
        EntNat b = new EntNat(scanner.nextInt());

        try {
            System.out.println(a + " + " + b + " = " + EntNat.somme(a, b));
            System.out.println(a + " - " + b + " = " + EntNat.difference(a, b));
            System.out.println(a + " * " + b + " = " + EntNat.produit(a, b));
        } catch (ErrCalcul err) {
            System.out.println("Resultat trop grand");
        }
    }
}
