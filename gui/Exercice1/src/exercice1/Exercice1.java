package exercice1;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;

class Window extends JFrame implements MouseListener {

    public Window() {
        super("My window");
        addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("Click");
    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("Pression");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("Relachement");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("Souris entré");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("Souris sortie");
    }
    
}

public class Exercice1 {

    public static void main(String[] args) {
        JFrame frame = new Window();
        frame.setVisible(true);
    }
}
