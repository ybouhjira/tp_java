package exercice6;

import static java.lang.Math.*;

class Point {
    // ATTRIBUTS

    private double x;
    private double y;

    // CONSTRUCTEUR
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // ACCESSEURS ET MUTATEURS
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ") ";
    }
}

class Circle {

    private double radius;
    private String color;
    private Point center;

    // CONSTRUCTEURS
    public Circle(Point center, double radius, String color) {
        this.radius = radius;
        this.color = color;
        this.center = center;
    }

    public Circle() {
        this(new Point(0, 0), 1, "red");
    }

    public Circle(double x, double y, double radius, String color) {
        this(new Point(x, y), radius, color);
    }

    // ACCESSEURS ET MUTATEURS
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    // METHODE AREA
    public double getArea() {
        return PI * pow(radius, 2);
    }

    // REDIFINITION DE TOSTRING
    @Override
    public String toString() {
        return center.toString() + ", radius: " + radius + ", color: " + color;
    }
}

public class Exercice6 {

    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(new Point(1,2), 10, "blue");
        Circle c3 = new Circle(3, 6.7, 9, "purple");
        
        c1.setColor("magenta");
        
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
    }
}
