package exercice1;

import java.util.ArrayList;
import java.util.Iterator;

class Point {

    private double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "point{" + "x=" + x + ", y=" + y + '}';
    }
}

public class Exercice1 {

    public static void main(String[] args) {
        ArrayList list = new ArrayList<>();

        list.add(12);
        list.add(0.5);
        list.add("Chaine de caractère");
        list.add(new Point(1, 2));

        Iterator itr = list.iterator();

        while (itr.hasNext()) {
            System.out.println(itr.next());
        }

    }
}
