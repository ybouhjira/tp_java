import java.util.Scanner;
import static java.lang.System.out;

class Exercice9 {
    public static void main(String[] args) {
        int nombre = 1 ;
        Scanner s = new Scanner(System.in);

        while (nombre != 0) {
            out.print("Donnez un nombre positif: \t");
            nombre = s.nextInt();

            String sqrt = String.valueOf(Math.sqrt(nombre)) ;
            if ( nombre > 0 )
                out.println("Sa racine carrée est : " + sqrt);
            else if (nombre < 0)
                out.println("SVP positif");
        }
    }
}
