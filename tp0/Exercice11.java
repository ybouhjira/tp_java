import java.util.Scanner;

public class Exercice11 {
    public static void main (String [] args)
    {
        Scanner scn = new Scanner(System.in);
        System.out.print("Entrez le nombre de valeurs :\t");
        int n = scn.nextInt();

        System.out.print("Entrez un nombre:\t");
        int max = scn.nextInt();
        for (int i=0; i<n-1; i++) {
            System.out.print("Entrez un nombre:\t");
            int num = scn.nextInt() ;
            if(num > max)
                max = num; 
        }
        System.out.println("MAX = " + max);
    }
}
