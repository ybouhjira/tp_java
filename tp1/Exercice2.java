import static java.lang.System.out;

public class Exercice2 {
    public static void main (String [] args)
    {
        out.println("Entrez le text : ");
        String text = Clavier.lireString(); 
        out.println("Entrez le mot: ");
        String word = Clavier.lireString(); 

        for (int i=0, l=word.length(); i<l; i++) {
            char c = word.charAt(i);
            out.println(c + " :\t" + matches(text, c));
        }
    }

    /**
     * @param string Chaine de caractere
     * @param c caractere
     * @return Nombre d'occurences de c dans str
     */
    public static int matches (String str, char c) {
        int count = 0;
        for (int i=0, l=str.length(); i<l; i++) 
            if(str.charAt(i) == c)
                count++;
        return count;
    }
}
