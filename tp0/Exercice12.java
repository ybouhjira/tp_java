import java.util.Scanner;
public class Exercice12 {
    public static void main (String [] args)
    {
        // Entree
        Scanner scn = new Scanner(System.in);
        System.out.print("Entrez n:\t");
        int n = scn.nextInt();
        
        // Calcul
        double sum = 0;
        for (int i=1; i<=n; i++)
            sum += 1/(double)i;

        // Affichage du resultat
        System.out.print("1 + 1/2 + ... + 1/" + n + " = " + sum );

    }
}
