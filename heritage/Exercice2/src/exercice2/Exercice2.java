package exercice2;

class A {

    public void hello() {
        System.out.println("Bonjour");
    }

    public void affiche() {
        System.out.println("Je suis objet de  A");
    }
}


class B extends A {

    @Override
    public void affiche() {
        System.out.println("Ja suis objet de B");
    }
    
}

public class Exercice2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        A a = new A();
        A b = new B();
        a.affiche();
        b.affiche();
    }
}
