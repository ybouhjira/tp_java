package exercice6;

import java.util.Scanner;

public class Exercice6 {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.println("Entrez la première chaine de caracteres : ");
        String str1 = scn.nextLine();

        System.out.println("Entrez la deuxième chaine de caracteres : ");
        String str2 = scn.nextLine();

        if (str1.indexOf(str2) >= 0) {
            System.out.println("Vrai");
        } else {
            System.out.println("Faux");
        }
    }
}
