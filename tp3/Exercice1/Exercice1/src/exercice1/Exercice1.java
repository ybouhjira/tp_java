package exercice1;

class Point {

    private char nom;
    private double x;
    private double y;

    /**
     * Constructeur
     * @param nom Nom du point
     * @param x Abscisse
     * @param y Ordonné
     */
    public Point(char nom, double x, double y) {
        this.nom = nom;
        this.x = x;
        this.y = y;
    }
    
    /**
     * @return Nom du point
     */
    public char getNom() {
        return nom;
    }

    /**
     * Change le nom
     * @param nom Nouveau nom
     */
    public void setNom(char nom) {
        this.nom = nom;
    }

    /**
     * @return Abscisse
     */
    public double getX() {
        return x;
    }

    /**
     * Change la valeur de X
     * @param x Nouvelle valeur
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return y ordonnée
     */
    public double getY() {
        return y;
    }

    /**
     * Change la valeur de y
     * @param y Nouvelle valeur 
     */
    public void setY(double y) {
        this.y = y;
    }
    
    /**
     * Fait la translation d'un point par un vecteur
     * @param dx Abscisse du vecteur 
     * @param dy Ordonné du vecteur
     */
    public void translate(int dx, int dy) {
        x += dx;
        y += dy;
    }
    
    public void affiche() {
        System.out.print(nom + "( " + x + " , " + y + " )");
    }
}

public class Exercice1 {

    public static void main(String[] args) {
        Point p = new Point('A',1,2);
        p.affiche();
        System.out.println();
        p.translate(2, 3);
        p.affiche();
    }
}
