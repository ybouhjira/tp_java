package exercice5;

class Amere {

    public void affiche() {
        System.out.println("Je suis un objet de type 'Amere'");
    }
}

public class Exercice5 {

    public static void main(String[] args) {
        Amere a = new Amere() {
            @Override
            public void affiche() {
                System.out.println("Je suis un anonyme derive de Amere");
            }
        };
        
        a.affiche();
    }
}
