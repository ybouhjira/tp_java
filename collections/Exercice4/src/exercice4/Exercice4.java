package exercice4;

import java.util.HashMap;
import java.util.Map;


class Point {

    private double x, y;
    private char nom;

    public Point(double x, double y, char nom) {
        this.x = x;
        this.y = y;
        this.nom = nom;
    }

    @Override
    public String toString() {
        return String.valueOf(nom) + "(" + x + "," + y+")"
                + " | clé : " + creerUneCle(); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public char getNom() {
        return nom;
    }

    public void setNom(char nom) {
        this.nom = nom;
    }

    public String creerUneCle() {
        return String.valueOf(nom).toUpperCase() + x + y;
    }
}

class ListPoint {

    private HashMap<String, Point> map = new HashMap<>();

    public void ajouterPoint(Point p) {
        map.put(p.creerUneCle(), p);
    }

    public void afficherLesPoints() {
        
        for (Map.Entry<String, Point> point : map.entrySet()) {
            System.out.println(point.getValue());
        }
    }

    public boolean rechercherUnPoint(Point p) {
        return map.containsValue(p);
    }

    public void supprimerUnPoint(Point p) {
        map.remove(p.creerUneCle());
    }
}

public class Exercice4 {

    public static void main(String[] args) {
        ListPoint list = new ListPoint();

        Point a = new Point(1, 2, 'A');
        list.ajouterPoint(a);
        list.ajouterPoint(new Point(2, 3, 'B'));
        list.ajouterPoint(new Point(4, 5, 'C'));

        if (list.rechercherUnPoint(a)) {
            System.out.println("A existe dans la liste");
        }

        list.afficherLesPoints();
    }
}
