import static java.lang.System.out;

public class Exercice2_2 {
    public static void main (String [] args)
    {
        out.println("Entrez un mot");
        StringBuffer word = new StringBuffer(Clavier.lireString());

        if(word.toString().equals( word.reverse().toString() ))
            out.println(word + " est un palindrome"); 
        else
            out.println(word + " n'est pas un palindrome");
    }
}
