package exercice1;

import java.util.Scanner;

class ErrConst extends Exception {
    private int value;

    /**
     * Constructeur
     * @param value Valeur qui a causé l'exception
     */
    public ErrConst(int value) {
        this.value = value;
    }

    /**
     * Accesseur
     * @return value Valeur qui a causé l'exception
     */
    public int getValue() {
        return value;
    }

    /**
     * Mutateur
     * @param value value Valeur qui a causé l'exception 
     */
    public void setValue(int value) {
        this.value = value;
    }
}

class EntNat {
    private int value;

    /**
     * Constructeur
     * @param value Entier naturel
     */
    public EntNat(int value) throws ErrConst {
        if(value < 0)
            throw new ErrConst(value);
        this.value = value;
    }

    /**
     * Accesseur
     * @return valeur 
     */
    public int getValue() {
        return value;
    }

    /**
     * Mutateur
     * @param value Entier naturel 
     */
    public void setValue(int value) throws ErrConst {
        if(value < 0)
            throw new ErrConst(value);
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
    
}

public class Exercice1 {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.println("Entrez un entier naturel");
        try {
            EntNat n = new EntNat(scn.nextInt());
            System.out.println("Vous avez entré : " + n.getValue());
        } catch (ErrConst ex) {
            System.out.println(ex.getValue() + "n'est pas un entier naturel");
        }
    }
}
