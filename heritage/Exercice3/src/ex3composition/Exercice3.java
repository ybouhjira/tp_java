package ex3composition;


class Forme {

    private double x, y;

    public Forme(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void deplace(double dx, double dy) {
        x += dx;
        y += dy;
    }

    public void affiche() {
        System.out.println("Point de coordonnees " + x + " " + y);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}

class Cercle {

    private double rayon;
    private Forme centre;

    public Cercle(int rayon, double x, double y) {
        centre = new Forme(x, y);
        this.rayon = rayon;
    }
    
    public void deplace(double dx, double dy) {
        centre.setX( centre.getX() + dx );
        centre.setY( centre.getY() + dy );
    }

    public double getX() {
        return centre.getX();
    }

    public double getY() {
        return centre.getY();
    }

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }

    public void affiche() {
        System.out.println(
                "Centre: (" + getX() + "," + getY() + ")"
                + "rayon: " + rayon);
    }
    
    public double surface() {
        return rayon * Math.pow(Math.PI , 2) ;
    }
    
    public double perimetre() {
        return 2 * rayon * Math.PI ;
    }
}

public class Exercice3 {

    public static void main(String[] args) {
        Cercle c = new Cercle(2, 1, 3);
        c.deplace(1, 1);
        System.out.println("Perimetre : " + c.perimetre());
        System.out.println("Surface : " + c.surface());
        c.affiche();
    }
}
