package exercice2;

class MyObject {
// ATTRIBUTS

    private static int maxId = 0;
    private int id;

//METHODES
    /*
     * Constructeur
     */
    public MyObject() {
        id = ++maxId;
    }

    /**
     * @return maxId
     */
    public static int getMaxId() {
        return maxId;
    }

    /**
     * @return Id
     */
    public int getId() {
        return id;
    }
}

public class Exercice2 {

    public static void main(String[] args) {
        MyObject[] objs = new MyObject[10];
        
        for (MyObject obj : objs) {
            obj = new MyObject();
            System.out.println(obj.getId());
        }

        System.out.println("MaxId : " + MyObject.getMaxId());
    }
}
