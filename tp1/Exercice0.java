import static java.lang.Math.*;
import static java.lang.System.out;

public class Exercice0 {
    public static void main (String [] args)
    {
        // Generer le nombre aleatoire
        int number = (int) (random() * 10) + 1;
        
        // Demander a l'utilisateur le nombre
        int answer = -1;
        while (answer != number) {
            out.print("Quel est le nombre secret?\t");
            answer = Clavier.lireInt();

            if (answer == number)
                out.println("Vous avez Gangné :)");
            else if(answer < number)
                out.println("C'est plus");
            else
                out.println("C'est moins");
        }
    }
}
