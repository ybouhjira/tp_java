package exercice4;

import java.util.ArrayList;
import java.util.Iterator;

abstract class Forme {
    
    private double x, y;
    
    public Forme(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public double getX() {
        return x;
    }
    
    public void setX(double x) {
        this.x = x;
    }
    
    public double getY() {
        return y;
    }
    
    public void setY(double y) {
        this.y = y;
    }
    
    public void affiche() {
        System.out.print("(" + x + "," + y + ")");
    }
    
    public void deplacer(double dx, double dy) {
        x += dx;
        y += dy;
    }
    
    public abstract double surface();
    
    public abstract double perimetre();
}

class Cercle extends Forme {
    
    private double rayon;

    // Constructeur
    public Cercle(double x, double y) {
        super(x, y);
    }
    
    @Override
    public void affiche() {
        System.out.print("Cercle");
        super.affiche();
        System.out.println();
    }
    
    @Override
    public double surface() {
        return rayon * Math.pow(Math.PI, 2);
    }
    
    @Override
    public double perimetre() {
        return 2 * Math.PI * rayon;
    }

    // Accesseurs et mutateurs
    public double getRayon() {
        return rayon;
    }
    
    public void setRayon(double rayon) {
        this.rayon = rayon;
    }
}

class Rectangle extends Forme {
    
    private double largeur, hauteur;
    
    public Rectangle(double largeur, double hauteur, double x, double y) {
        super(x, y);
        this.largeur = largeur;
        this.hauteur = hauteur;
    }
    
    public double getLargeur() {
        return largeur;
    }
    
    public void setLargeur(double largeur) {
        this.largeur = largeur;
    }
    
    public double getHauteur() {
        return hauteur;
    }
    
    public void setHauteur(double hauteur) {
        this.hauteur = hauteur;
    }
    
    @Override
    public void affiche() {
        System.out.print("Rectangle ");
        super.affiche();
        System.out.print("Largeur : " + largeur);
        System.out.print("Hauteur : " + hauteur);
        System.out.println();
    }
    
    @Override
    public double surface() {
        return largeur * hauteur;
    }
    
    @Override
    public double perimetre() {
        return 2 * largeur + 2 * hauteur;
    }
}

class Carre extends Forme {

    private double cote;
    
    public Carre(double cote, double x, double y) {
        super(x, y);
        this.cote = cote;
    }
    
    @Override
    public double surface() {
        return cote * cote;
    }
    
    @Override
    public double perimetre() {
        return 2 * cote;
    }
    
    @Override
    public void affiche() {
        System.out.print("Carré ");
        super.affiche();        
        System.out.println(" - Coté : " + cote);
    }
}

class ListeDesForme extends ArrayList<Forme> {

    public void toutDeplacer(double dx, double dy) {
        Iterator<Forme> itr = iterator();
        
        while (itr.hasNext()) {
            itr.next().deplacer(dx, dy);
        }
    }
    
    public double surfaceTotale () {
        double surface  = 0 ;
        Iterator<Forme> itr = iterator();
        
        while (itr.hasNext()) {
            surface += itr.next().surface();
        }
        
        return surface;
    }
    
    public double perimetreTotal () {
        double perimetre  = 0 ;
        Iterator<Forme> itr = iterator();
        
        while (itr.hasNext()) {
            perimetre += itr.next().perimetre();
        }
        
        return perimetre;
    }
    
    public void afficheTout() {
        Iterator<Forme> itr = iterator();
        
        while(itr.hasNext()) {
            itr.next().affiche();
        }
    }
}

public class Exercice4 {
    
    public static void main(String[] args) {
        ListeDesForme list = new ListeDesForme();
        list.add(new Cercle(1, 0));
        list.add(new Rectangle(1, 2, 3, 4));
        list.add(new Carre(4, 0, 0));
        
        System.out.println("Surface totale : " + list.surfaceTotale());
        System.out.println("Périmetre total : " + list.perimetreTotal());
        
        list.afficheTout();
    }
}
