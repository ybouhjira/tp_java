package exercice3;

import java.util.Scanner;

class ErrPaier extends Exception {

    private int value;

    public ErrPaier(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

class EntPaire {

    private int value;

    public EntPaire(int value) throws ErrPaier {
        if (value % 2 != 0) {
            throw new ErrPaier(value);
        }
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) throws ErrPaier {
        if (value % 2 != 0) {
            throw new ErrPaier(value);
        }
        this.value = value;
    }
}

public class Exercice3 {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.print("Entrez un nombre paire:\t");
        try {
            EntPaire p = new EntPaire(scn.nextInt());
            System.out.println("Vous avez entré : " + p.getValue());
        } catch (ErrPaier ex) {
            System.out.println(ex.getValue() + " est impaire.");
        }
    }
}
