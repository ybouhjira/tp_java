package yb.matrix;

import static yb.matrix.MatrixMathException.Operation;

/**
 * Cette classe permet de faire la somme, la soustraction et le produit des
 * matrices
 *
 * @author Youssef Bouhjira
 */
public class Matrix {

// ATTRIBUTS
    private double[][] data;
    private int rowCount;
    private int columnCount;

    /**
     * Constructeur
     *
     * @param rowCount Nombre de lignes
     * @param columnCount Nombre de colonnes
     */
    public Matrix(int rowCount, int columnCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        this.data = new double[rowCount][columnCount];
    }

// METHODES
    /**
     * Constructeur : Crée une matrice initialisée par initValue
     *
     * @param rowCount Nombre de lignes
     * @param columnCount Nombre de colonnes
     * @param initValue Valeur d'initialisation
     */
    public Matrix(int rowCount, int columnCount, double initValue) {
        this(rowCount, columnCount);
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                this.data[i][j] = initValue;
            }
        }
    }

    /**
     * Crée une matrice égal à la matrice other
     *
     * @param other Autre matrice
     */
    public Matrix(Matrix other) {
        this(other.getRowCount(), other.getColumnCount());
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                data[i][j] = other.getValue(i, j);
            }
        }
    }

    /**
     * @return Représentation de la matrice en chaîne de caractères
     */
    @Override
    public String toString() {
        StringBuilder strBuf = new StringBuilder();
        for (double[] row : data) {
            for (double val : row) {
                strBuf.append(val).append(" ");
            }
            strBuf.append('\n');
        }
        return strBuf.toString();
    }

// GETTRES & SETTERS
    /**
     * @return Le nombre de lignes
     */
    public int getRowCount() {
        return this.rowCount;
    }

    /**
     * @return Le nombre de colonnes
     */
    public int getColumnCount() {
        return this.columnCount;
    }

    /**
     * @param row Ligne
     * @param col Colonne
     * @return Valeur dans la case (row,col)
     */
    public double getValue(int row, int col) {
        assert row >= 0 && row < data.length
                && col >= 0 && col < data[ row].length;
        return this.data[ row][ col];
    }

    /**
     * Change une case de la matrice
     *
     * @param row Ligne
     * @param col Colonne
     * @param value Nouvelle valeur
     */
    public void setValue(int row, int col, double value) {
        assert row <= 0 && row < data.length
                && col >= 0 && col < data[ row].length;
        this.data[ row][col] = value;
    }

// METHODES DE CALCUL 
    /**
     * Fait l'addition de deux matrices
     *
     * @param other matrice avec laquelle faire l'addition
     * @return Matrice résultat
     * @throws MatrixMathException : Si les deux matrices n'ont pas le même
     * nombre de lignes et le même nombre de colonnes
     */
    public Matrix add(Matrix other) throws MatrixMathException {
        // Exception
        if (other.getColumnCount() != this.columnCount
                || other.getRowCount() != this.rowCount) {
            throw new MatrixMathException(MatrixMathException.Operation.ADD);
        }
        Matrix result = new Matrix(rowCount, columnCount);

        // Calcul
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                result.setValue(i, j, data[i][j] + other.getValue(i, j));
            }
        }
        return result;
    }

    /**
     * Produit d'une matrice avec un nombre
     *
     * @param value nombre
     * @return Matrice résultat
     */
    public Matrix multiply(double value) {
        Matrix result = new Matrix(this);
        
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                result.setValue(i, j, value * result.getValue(i, j));
            }
        }
        return result;
    }
    
    /**
     * Calcule le produit de deux matrices
     * @param other Autre matrice
     * @return Matrice résultat
     * @throws MatrixMathException Si le nombre de colonnes de la première 
     * matrice est différent du nombre de lignes de la deuxième matrice
     */
    public Matrix multiply(Matrix other) throws MatrixMathException {
        if (columnCount != other.rowCount) {
            throw new MatrixMathException(Operation.MULTIPPLY);
        }
        Matrix result = new Matrix(rowCount, other.getColumnCount(), 0);
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < other.getColumnCount(); j++) {
                double current = 0;
                for (int x = 0; x < columnCount; x++) {
                    current += data[i][x] * other.getValue(x, j);
                }
                result.setValue(i, j, current );
            }
        }
        return result;
    }

    /**
     * Calcule la différence de deux matrice
     *
     * @param other Autre matrice
     * @return Matrice résultat
     * @throws MatrixMathException Si Les deux matrices n'ont pas les mêmes
     * dimensions
     */
    public Matrix substract(Matrix other) throws MatrixMathException {
        // M1 - M2 = M1 + (-1 * M2)
        return this.add(other.multiply(-1));
    }
}