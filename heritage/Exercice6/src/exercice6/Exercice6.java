package exercice6;

import static java.lang.Math.*;

class Point {

    private double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" + "x=" + x + ", y=" + y + '}';
    }
}

class Line {

    private Point debut, fin;

    public Line(Point debut, Point fin) {
        this.debut = debut;
        this.fin = fin;
    }

    public Line(double x1, double y1, double x2, double y2) {
        this.debut = new Point(x1, y1);
        this.fin = new Point(x2, x2);
    }

    public Point getDebut() {
        return debut;
    }

    public void setDebut(Point debut) {
        this.debut = debut;
    }

    public Point getFin() {
        return fin;
    }

    public void setFin(Point fin) {
        this.fin = fin;
    }

    @Override
    public String toString() {
        return "Line{ Debut: " + debut.toString()
                + " , Fin:" + fin.toString() + "}";
    }

    public double getLength() {
        return sqrt(pow(debut.getX() - fin.getY(), 2)
                + pow(debut.getY() - fin.getY(), 2));
    }
}

public class Exercice6 {

    public static void main(String[] args) {
        Line line = new Line(0,0, 1, 1);
        System.out.println(line);
        System.out.println("Longeur : " + line.getLength());
    }
}
