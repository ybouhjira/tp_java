package exercice1;

class Point {
// ATTRIBUTS

    private double x; // abscisse
    private double y; // ordonnee

    // METHODES
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void affiche() {
        System.out.println("(" + x + "," + y + ")");
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void deplace(double dx, double dy) {
        x += dx;
        y += dy;
    }

    public double abscisse() {
        return x;
    }

    public double ordonnee() {
        return y;
    }
}

class PointNom extends Point {

    private char nom;

    public PointNom(char nom, double x, double y) {
        super(x, y);
        this.nom = nom;
    }

    public char getNom() {
        return nom;
    }

    public void setNom(char nom) {
        this.nom = nom;
    }

    @Override
    public void affiche() {
        System.out.print(nom);
        super.affiche();
    }
}

public class Exercice1 {

    public static void main(String[] args) {
        Point p = new PointNom('P', 0, 0);
        System.out.println("Point P : ");
        p.affiche();

        System.out.println("P aprés deplacement");
        p.deplace(1, 1);
        p.affiche();
    }
}
