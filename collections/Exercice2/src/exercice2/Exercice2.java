package exercice2;

import java.util.ArrayList;
import java.util.Iterator;

class Point {

    private double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" + "x=" + x + ", y=" + y + '}';
    }
}

class ListPoint {

    private ArrayList<Point> list = new ArrayList<>();

    public void ajouterUnPoint(Point p) {
        list.add(p);
    }

    public void afficherLesPoints() {
        Iterator<Point> itr = list.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }
}

public class Exercice2 {

    public static void main(String[] args) {
        ListPoint list = new ListPoint();
        
        list.ajouterUnPoint(new Point(1,2));
        list.ajouterUnPoint(new Point(2,3));
        list.ajouterUnPoint(new Point(3,4));
        
        list.afficherLesPoints();
    }
}
