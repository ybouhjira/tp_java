import java.util.Scanner;
import static java.lang.System.out;

public class Exercice8For {
    public static void main (String[] arg) {
        Scanner s = new Scanner(System.in);

        out.print("Combien de nombre voulez vous ? \t");
        int n = s.nextInt(), somme = 0 ;

        for(int i=0; i<n; i++) {
            out.print("Nombre " + String.valueOf(i) + " : \t" );
            somme += s.nextInt();
        }

        out.println("La somme des nombres est :\t"+String.valueOf(somme));
    }
}
