package exercice2;

interface Affichable {
    void affiche() ;
}


class A1 implements Affichable {

    public void hello() {
        System.out.println("Bonjour");
    }

    @Override
    public void affiche() {
        System.out.println("Je suis un objet de A");
    }
}


class B1 implements Affichable {

    @Override
    public void affiche() {
        System.out.println("Je suis un objt de B");
    }
    
}

public class InterfaceTest {
    public static void main(String[] args) {
        Affichable a = new A1();
        Affichable b = new B1();
        a.affiche();
        b.affiche();
    }
    
}
