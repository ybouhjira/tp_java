import java.util.Scanner;
import static java.lang.System.out;

public class Exercice6 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        out.print("Entrez a:\t");
        int a = s.nextInt();
        out.print("Entrez b:\t");
        int b = s.nextInt();

        if( a > b )
            out.println("a est supérieur à b");
        else if (a < b)
            out.println("a est inférieur à b");
        else
            out.println("a est égal à b");
    }
}
