package exercice6;

class Etudiant {

    private String nom;
    private String prenom;
    private int promotion;

    public Etudiant(String nom, String prenom, int promotion) {
        this.nom = nom;
        this.prenom = prenom;
        this.promotion = promotion;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getPromotion() {
        return promotion;
    }

    public void setPromotion(int promotion) {
        this.promotion = promotion;
    }

    @Override
    public String toString() {
        return nom + " " + prenom + ", promotion: " + promotion;
    }
}

public class Exercice6 {

    public static void main(String[] args) {
        Etudiant etudiant = new Etudiant("Bouhjira", "Youssef", 2013);
        System.out.println(etudiant);
    }
}
