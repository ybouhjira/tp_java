package tp1.exercice5;

import java.util.Scanner;
import yb.matrix.Matrix;
import yb.matrix.MatrixMathException;

/**
 * Exercice 5 - TP 1
 *
 * @author Youssef Bouhjira
 */
public class Exercice5 {

    /**
     * main
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);

        // PREMIERE MATRICE
        System.out.println("Première matrice : ");
        Matrix m1 = new Matrix(readMatrix());
        
        // DEUXIEME MATRICE
        System.out.println("Première matrice : ");
        Matrix m2 = new Matrix(readMatrix());

        // SOMME
        try {
            System.out.println("M1 + M2 = ");
            System.out.println(m1.add(m2));
        } catch (MatrixMathException exc) {
            System.out.println("Erreur: Les deux matrices n'ont pas les "
                    + "mêmes dimensions");
        }
        
        // SOMME
        try {
            System.out.println("M1 - M2 = ");
            System.out.println(m1.substract(m2));
        } catch (MatrixMathException exc) {
            System.out.println("Erreur: Les deux matrices n'ont pas les "
                    + "mêmes dimensions");
        }
        
        // SOMME
        try {
            System.out.println("M1 * M2 = ");
            System.out.println(m1.add(m2));
        } catch (MatrixMathException exc) {
            System.out.println("Erreur: Impossible de calculer le produit "
                    + "des deux matrices");
        }
    }

    /**
     * Lit une matrice entrée au clavier
     * @return Matrice lut
     */
    private static Matrix readMatrix() {
        Scanner scn = new Scanner(System.in);

        // Lire le nombre de lignes et le nombre de colonnes
        System.out.print("Nombres de lignes:\t");
        int rowCount = scn.nextInt();
        System.out.print("Nombres de lignes:\t");
        int colCount = scn.nextInt();

        Matrix matrix = new Matrix(rowCount, colCount);

        scn.useDelimiter("\\s");
        for (int i = 0; i < rowCount; i++) {
            System.out.print("ligne " + String.valueOf(i) + ":\t");
            
            for (int j = 0; j < colCount; j++) {
                matrix.setValue(i, j, scn.nextDouble());
            }
        }
        return matrix;
    }
}