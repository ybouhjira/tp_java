/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package yb.matrix;

/**
 * Exception lancée par la classe matrice si on essaye de faire une opération
 * mathématique invalide entre deux matrice, par exemple : faire l'addition de
 * deux matrice qui n'ont pas les mêmes dimensions
 *
 * @author Youssef Bouhjira
 */
public class MatrixMathException extends Exception {

// TYPES
    /**
     * Type de l'opération qui & causé le problème
     */
    public enum Operation {

        ADD, SUBSTRACT, MULTIPPLY
    }
// ATTRIBUTS
    private Operation opeartion;

// METHODES
    public MatrixMathException(Operation opeartion) {
        this.opeartion = opeartion;
    }

// Accessceurs et modificateurs
    /**
     * Accessceur
     * @return Opération qui à causé l'exception
     */
    public Operation getOpeartion() {
        return opeartion;
    }

    /**
     * modificateur
     * @param opeartion : Nouvelle valeur pour l'opération 
     */
    public void setOpeartion(Operation opeartion) {
        this.opeartion = opeartion;
    }
    
}
