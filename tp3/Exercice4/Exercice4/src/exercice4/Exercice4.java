package exercice4;

class Point {
// ATTRIBUTS
    private double x; // abscisse
    private double y; // ordonnee

// METHODES
    /**
     * Constructeur
     * @param x
     * @param y 
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void deplace(double dx, double dy) {
        x += dx;
        y += dy;
    }

    public double abscisse() {
        return x;
    }

    public double ordonnee() {
        return y;
    }
}

public class Exercice4 {

    public static void main(String[] args) {
        // TODO code application logic here
    }
}
