import static java.lang.Math.*;
import static java.lang.System.out;

class Exercice3 {
    public static void main (String [] args)
    {
        // Lire le nombre de valeurs 
        out.print("Entrez un nombre:\t");
        int count = Clavier.lireInt();  

        // Calculer les carrés
        for (int i=0; i<count ;i++) {
            int nbr = 2 * i + 1 ;
            out.println(nbr + " à pour carré " + (long)pow(nbr,2)) ;
        }
       
    }
} 
