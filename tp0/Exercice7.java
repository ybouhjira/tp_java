import java.util.Scanner;
import static java.lang.System.out;

public class Exercice7 {
    public static void main (String[] args) {
        out.println("Entrez un nombre en 0 et 4:  ");

        // Lecture du nombre
        Scanner s = new Scanner(System.in);
        int nombre = s.nextInt();

        String nom = "";
        switch(nombre) {
            case 0:
                nom = "Zéro";
                break;
            case 1:
                nom = "Un";
                break;
            case 2:
                nom = "Deux";
                break;
            case 3:
                nom = "Trois";
                break;
            case 4:
                nom = "Quatre";
                break;
            default:
                nom = "pas compris entre 0 est 4";
        }
        //Affichage du resultat:
        out.println(String.valueOf(nombre) + " : " + nom);
    }
}

