import static java.lang.System.out;

class Exercice4 {
    public static void main (String [] args)
    {
        out.print("Combien de valeurs?\t");
        int number = Clavier.lireInt();

        // Creation du tableau
        int[] array = new int[number];

        // Remplissage du tableau
        for (int i=0, l=array.length; i<l; i++) {
            out.print("Entrez un nombre:\t");
            array[i] = Clavier.lireInt();
        }

        int sum = 0, min = array[0], max = array[0];
        for (int i=0, l=array.length; i<l; i++) {
            // Moyenne
            sum += array[i];

            // Max
            if(array[i] > max)
                max = array[i];

            // Min
            if(array[i] < min)
                min = array[i];
        } 
        out.println("Moyenne:\t" + (double)sum/number);
        out.println("Maximum:\t" + max);
        out.println("Moyenne:\t" + min);
    }
}
