import java.util.Scanner;
import static java.lang.System.out;

public class Exercice8While {
    public static void main (String[] arg) {
        Scanner s = new Scanner(System.in);

        out.print("Combien de nombre voulez vous ? \t");
        int n = s.nextInt(), somme = 0 ;

        int i=0;
        while( i < n) {
            out.print("Nombre " + String.valueOf(i) + " : \t" );
            somme += s.nextInt();
            i++;
        }

        out.println("La somme des nombres est :\t"+String.valueOf(somme));
    }
}
