import java.util.Scanner;

public class Exercice14 {
    public static void main (String [] args)
    {
        Scanner scn = new Scanner(System.in);
        System.out.print("Entrez epsilon : ");
        double epsilon = scn.nextDouble();

        double Un = 1, Un1;
        do {
            Un1 = Un + 1 / Un;
            Un = Un1;
        } while (Math.abs(Un1 - Un) > epsilon);

        System.out.println("lim = " + Un1);
    }
}
