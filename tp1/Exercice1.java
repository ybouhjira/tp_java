import static java.lang.System.out;
import java.util.Arrays;
import java.util.Comparator;

/**
 * @brief Cette class implemete l'interface Comparator
 * pour avoir un comparateur qui permet de comparer les chaines
 * sans prendre compte de la case
 */
class StringComparator implements Comparator<String> {
    public int compare(String s1, String s2) {
        return s1.compareToIgnoreCase(s2);
    }
}

public class Exercice1 {
    public static void main (String [] args)
    {
        // Recuperez les mots
        out.print("Combien de mots?\t");
        int count = Clavier.lireInt();
        String[] words = new String[count];

        out.println("Donnez vos mots : ");
        for (int i=0; i<count; i++)
             words[i] = Clavier.lireString();

        // Trier
        out.println(" -- Liste par order alphabétique : ");
        StringComparator comparator = new StringComparator();
        Arrays.sort(words, comparator);

        // Affichage du resultat
        for (String word: words) 
            out.println(word);
    }
}
